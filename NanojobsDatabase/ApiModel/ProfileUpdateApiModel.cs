﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
  public class ProfileUpdateApiModel
    {
        public string  Latitude { get; set; }
        public string Longitude { get; set; }
        public string DeviceId { get; set; }
    }
}
