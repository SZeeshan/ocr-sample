﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.ApiModel
{
   public class ActivityResponseModel
    {
        public ActivityDetails buyer { get; set; }
        public ActivityDetails seller { get; set; }
    }
}
