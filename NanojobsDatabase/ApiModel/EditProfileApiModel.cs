﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
   public class EditProfileApiModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public string ProfilePicture { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
