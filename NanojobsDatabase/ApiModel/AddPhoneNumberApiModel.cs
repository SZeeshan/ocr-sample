﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.ApiModel
{
    public class AddPhoneNumberApiModel
    {
        public string PhoneNumber { get; set; }
    }
}
