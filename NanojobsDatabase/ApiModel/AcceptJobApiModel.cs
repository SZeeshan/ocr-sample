﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
   public class AcceptJobApiModel
    {
        public Guid JobId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
