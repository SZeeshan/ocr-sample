﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
   public class SocketReturnModel
    {
        public dynamic DATA { get; set; }
        public string CHANNEL { get; set; }
    }
}
