﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.ApiModel
{
    public class SuccessReturnModel
    {
        public static ResponseModel SuccessModel(dynamic data, string message)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.success = true;
            responseModel.data = data;
            responseModel.message = message;

            return responseModel;
        }

        public static ResponseModel FailureModel(string message)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.success = false;
            responseModel.data = null;
            responseModel.message = message;

            return responseModel;
        }
        public SuccessReturnModel(dynamic desc)
        {
            description = desc;
        }

        public dynamic description { get; set; }
    }
}
