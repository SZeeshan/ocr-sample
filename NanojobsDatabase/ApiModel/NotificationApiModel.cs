﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
    public class NotificationApiModel
    {
        public int CategoryId { get; set; }
        public bool Status { get; set; }
    }
}
