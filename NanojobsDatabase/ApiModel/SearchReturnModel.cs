﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
    public class SearchReturnModel
    {
        public Guid Id { get; set; }
        public int CategoryId { get; set; }
        public string Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PostalCode { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string UserName { get; set; }
        public string ProfilePicture { get; set; }
        public string CategoryName { get; set; }
        public int? Price { get; set; }
        public double TotalDistanceKm { get; set; }
        public string PopUpLink { get; set; }
        public Guid UserId { get; set; }
    }
}
