﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.ApiModel
{
   public class JobDetailListingModel
    {
        public Guid Id { get; set; }
        public string Location { get; set; }
        public int JobDateTime { get; set; }
        public string JobStatus { get; set; }
        public string CategoryName { get; set; }
        public double TotalPrice { get; set; }
        public string JobPosterUsername { get; set; }
        public string JobAcceptUsername { get; set; }
        public string TotalDuration { get; set; }
        public string JobDescription { get; set; }
        public double BasePrice { get; set; }
        public double AdditionalCharges { get; set; }
        public double Commission { get; set; }
        public int CategoryId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string  ImageLink { get; set; }
    }
}
