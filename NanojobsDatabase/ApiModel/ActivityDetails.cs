﻿using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.ApiModel
{
   public class ActivityDetails
    {
        public string Name { get; set; }
        public string Longitude { get; set; }
        public Guid JobId { get; set; }
        public Guid SellerId { get; set; }
        public Guid BuyerId { get; set; }
        public string CategoryName { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public string Latitude { get; set; }
        public string PostalCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? CompleteOn { get; set; }
        public int OfferId { get; set; }
        public string ProfilePicture { get; set; }
        public string UserLatitude { get; set; }
        public string UserLongitude { get; set; }
        public int CategoryId { get; set; }
    }
}
