﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.ApiModel
{
   public class JobDetailResponseModel
    {
        public List<JobDetailListingModel> Done { get; set; }
        public List<JobDetailListingModel> Posted { get; set; }
    }
}
