﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
   public class AcceptJobReturnModel
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ProfilePic { get; set; }
        public string Name { get; set; }
       
    }
}
