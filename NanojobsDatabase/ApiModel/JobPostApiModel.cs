﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
    public class JobPostApiModel
    { 
        public int CategoryId { get; set; }
        public string Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PostalCode { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string PaypalEmail { get; set; }
        public string Nonce { get; set; }
    }
}
