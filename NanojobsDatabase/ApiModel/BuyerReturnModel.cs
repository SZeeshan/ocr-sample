﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
  public class BuyerReturnModel
    {
        public string Buyername { get; set; }
        public string ProfilePic { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Guid JobId { get; set; }
        public string CategoryName { get; set; }
        public string Status { get; set; }
    }
}
