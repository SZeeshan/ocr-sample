﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.ApiModel
{
   public class SellerReturnModel
    {
        public string sellername { get; set; }
        public string ProfilePic { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Guid JobId { get; set; }
        public string CategoryName { get; set; }
        public string Status { get; set; }
    }
}
