﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanojobsDatabase.ApiModel
{
   public class UserReturnModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsVerified { get; set; }
        public string Location { get; set; }
        public string ProfilePicture { get; set; }
        public string DeviceId { get; set; }
        public string Longitiude { get; set; }
        public string Latitude { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public double? Balance { get; set; }
        public bool? IsValidPaypal { get; set; }
        public string  PaypalEmailAddress { get; set; }
        public List<UserCarddetailsModel> Usercarddetails { get; set; }
    }
    public class UserCarddetailsModel
    {
        public int Id { get; set; }
        public string CardType { get; set; }
        public bool IsDefault { get; set; }
        public string PaypalEmail { get; set; }
        public string CardNumber { get; set; }
    }
}
