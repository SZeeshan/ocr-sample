﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Models
{
    public class CustomUserProfile
    {
        public Guid Id { get; set; }
        public string DeviceId { get; set; }
        public string Longitiude { get; set; }
        public string Latitude { get; set; }
        public bool Distance { get; set; }
        public bool UserNotification { get; set; }
        public int CategoryId { get; set; }
    }
}
