﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{
   public class PaymentLogModel
    {
        public int Id { get; set; }
        public Guid JobPosterUserId { get; set; }
        public Guid JobAcceptorUserId { get; set; }
        public string Location { get; set; }
        public DateTime? JobDateTime { get; set; }
        public string JobStatus { get; set; }
        public Guid JobId { get; set; }
        public string CategoryName { get; set; }
        public double TotalPrice { get; set; }
        public bool PaymentToJobAcceptor { get; set; }
        public bool PaymentToJobPoster { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
