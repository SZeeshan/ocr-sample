﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Models
{
    [Table("Categories")]
    public class Categories
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Wage { get; set; }
        public string SelectedLink { get; set; }
        public string UnSelectedLink { get; set; }
        public string JobSearchLink { get; set; }
        public string PopUpLink { get; set; }
    }
}
