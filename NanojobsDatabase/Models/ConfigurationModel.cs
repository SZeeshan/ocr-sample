﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{
    [Table("Configuration")]
   public class ConfigurationModel
    {
        public int Id { get; set; }
        public string Tittle { get; set; }
        public double CompanyFees { get; set; }
    }
}
