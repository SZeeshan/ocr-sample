﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NanojobsDatabase.Models
{
    [Table("SignalRConnection")]
    public class SignalRConnectionModel
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string ConnectionId { get; set; }
        public string UserAgent { get; set; }
        public bool IsConnected { get; set; }
    }
}
