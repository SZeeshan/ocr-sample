﻿using Nanojobs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NanojobsDatabase.Models
{
    [Table("PostJobs")]
     public class PostJobs
    {
        [Key]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")] 
        public Categories categories { get; set; }

        [ForeignKey("UserId")]
        public UserProfile userprofile { get; set; }

        public string Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PostalCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? CompleteOn { get; set; }
        public string Status { get; set; }

        public int OfferId { get; set; }

        [ForeignKey("OfferId")]
        public JobOffers joboffer { get; set; }
    }
}
