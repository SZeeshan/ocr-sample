﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{

    [Table("UserCategory")]
   public class UserCategory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category dbCategory { get; set; }
        public int CategoryId { get; set; }

    }
}
