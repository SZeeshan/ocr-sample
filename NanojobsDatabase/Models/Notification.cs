﻿using Nanojobs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NanojobsDatabase.Models
{
    [Table("Notification")]
   public class Notification
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserId { get; set; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Categories category { get; set; }
        public bool Status { get; set; }
    }
}
