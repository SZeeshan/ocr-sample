﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{
    [Table("UserCardDetails")]
   public class UserCardDetails
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string CardType { get; set; }
        public bool IsDefault { get; set; }
        public string PayPalEmal { get; set; }
        public string CardNumber { get; set; }
        public string PaypalCustomerId { get; set; }
        public string Token { get; set; }
    }
}
