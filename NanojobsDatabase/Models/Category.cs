﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{
    [Table("Category")]
   public class Category
    {
        public int Id { get; set; }
        public string CategoriesName { get; set; }
        public string CategoryValues { get; set; }
    }
}
