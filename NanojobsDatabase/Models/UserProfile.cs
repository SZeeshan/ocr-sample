﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Models
{
    [Table("UserProfile")]
    public class UserProfile
    {
        // here you can write the properties related to db
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsVerified { get; set; }
        public string PasswordHash { get; set; }
        public string Location { get; set; }
        public string ProfilePicture { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string DeviceId { get; set; }
        public string PaypalToken { get; set; }
        public string PaypalCustomerId { get; set; }
        public string Longitiude { get; set; }
        public string Latitude { get; set; }
        public bool IsNotify { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CardNumber { get; set; }
        public string ZipCode { get; set; }
        public string CardMonth { get; set; }
        public string CardYear { get; set; }
        public string UserName { get; set; }
        public string PaypalEmail { get; set; }
        public bool? IsValidPaypal { get; set; }
        public bool? IsProfile { get; set; }
        public double? Balance { get; set; }
    }
}
