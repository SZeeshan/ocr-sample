﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NanojobsDatabase.Models
{
    [Table("UserPreference")]
   public class UserPreference
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CustomValues { get; set; }
    }
}
