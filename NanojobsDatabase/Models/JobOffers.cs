﻿using Nanojobs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NanojobsDatabase.Models
{
    [Table("JobOffers")]
   public class JobOffers
    {
        [Key]
        public int Id { get; set; }
        public Guid BuyerId { get; set; }

        [ForeignKey("BuyerId")]
        public UserProfile buyername { get; set; }

        public Guid PostJobId { get; set; }
        public double Price { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? JobStarted { get; set; }
        public DateTime? JobCompleted { get; set; }
    }
}
