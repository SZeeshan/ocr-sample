﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Nanojobs.Infrastructure
{
    public class ServiceResult<T>: ServiceResult
    {
        public ServiceResult(bool isValid, IList<ValidationFailure> errors, T value) : base(isValid, errors)
        {
            Value = value;
        }

        public ServiceResult(bool isValid, Nanojobs statusCode, IList<ValidationFailure> errors, T value) : base(isValid, statusCode, errors)
        {
            Value = value;
        }

        public T Value { get; }

    }

    public class ServiceResult
    {
        public ServiceResult(bool isValid, IList<ValidationFailure> errors)
        {
            IsValid = isValid;
            StatusCode = isValid ? Nanojobs.Ok : Nanojobs.BadRequest;
            Errors = errors;
        }

        public ServiceResult(bool isValid, Nanojobs statusCode, IList<ValidationFailure> errors)
        {
            IsValid = isValid;
            StatusCode = statusCode;
            Errors = errors;
        }

        public bool IsValid { get; }
        public Nanojobs StatusCode { get; }
        public IList<ValidationFailure> Errors { get; }

    }

    public enum Nanojobs
    {
        Ok,
        BadRequest,
        Conflict,
        InternalServerError
    }
}

