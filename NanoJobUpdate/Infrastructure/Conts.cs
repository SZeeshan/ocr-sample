﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Infrastructure
{
    public class Conts
    {
        public static readonly string PENDING = "Pending";
        public static readonly string INPROGRESS = "In Progress";
        public static readonly string STARTED = "Started";
        public static readonly string COMPLETED = "Completed";
    }
    public enum STATUS
    {
        Pending,
        InProgress,
        Started,
        Completed
    }
}
