﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using NanojobsDatabase.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Infrastructure
{
    public class ChatHub : Hub
    {
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

        public static void Server_Send(string message)
        {
            using (NanoDbContext db = new NanoDbContext())
            {
                var connectionList = db.signalR.Where(s => s.IsConnected).ToList();

                foreach (var connection in connectionList)
                {
                    var aa = hubContext.Clients.Client(connection.ConnectionId).broadcastMessage(message);
                }

                hubContext.Clients.All.broadcastMessage(message);
            }
        }

        public static void Server_Send_Channel(string user_id, string message)
        {
            using (NanoDbContext db = new NanoDbContext())
            {
                Guid guid = new Guid(user_id);
                var connectionList = db.signalR.Where(s => s.UserId == guid && s.IsConnected==true).ToList();

                if (connectionList.Count != 0)
                {
                    foreach (var connection in connectionList)
                    {
                        hubContext.Clients.Client(connection.ConnectionId).broadcastMessage(message);
                    }
                }
            }
        }

        [System.Web.Http.Authorize]
        public override Task OnConnected()
        {
            using (NanoDbContext db = new NanoDbContext())
            {
                try
                {
                    // string UserId = User.Identity.GetUserId();
                    var version = Context.QueryString["authorization"];
                    string userId = Context.User.Identity.GetUserId();
                    string userName = Context.User.Identity.GetUserName();
                    Guid guid = new Guid(userId);
                    if (userId != null)
                    {
                        SignalRConnectionModel signalRConnectionModel = new SignalRConnectionModel()
                        {
                            ConnectionId = Context.ConnectionId,
                            IsConnected = true,
                            UserId = guid,
                            UserAgent = Context.Request.Headers["User-Agent"],
                        };

                        db.signalR.Add(signalRConnectionModel);
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    var a = 1;
                }
            }

            return base.OnConnected();
        }

        [System.Web.Http.Authorize]
        public override Task OnDisconnected(bool stopCalled)
        {
            using (NanoDbContext db = new NanoDbContext())
            {
                try
                {
                    var userId = Context.User.Identity.GetUserId();

                    SignalRConnectionModel signalRConnectionModel = db.signalR.Where(s => s.ConnectionId == Context.ConnectionId).FirstOrDefault();
                    if (signalRConnectionModel != null)
                    {
                        signalRConnectionModel.IsConnected = false;
                        db.Entry(signalRConnectionModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    var a = 1;
                }
            }

            return base.OnDisconnected(stopCalled);
        }
    }

}
