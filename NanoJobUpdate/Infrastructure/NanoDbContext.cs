﻿using Microsoft.AspNet.Identity.EntityFramework;
using Nanojobs.Models;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Infrastructure
{
    public class NanoDbContext : DbContext
    {
        public NanoDbContext()
             : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static NanoDbContext Create()
        {
            return new NanoDbContext();
        }
        //new model
        public DbSet<Category> getCategories { get; set; }
        public DbSet<UserDetails> userDetails { get; set; }
        public DbSet<UserPreference> userPreferences{ get; set; }
        public DbSet<UserCategory> userCategories { get; set; }



        public DbSet<UserProfile> userprofile { get; set; }
        public DbSet<UserPhone> userphone { get; set; }
        public DbSet<UserCardDetails> usercarddetails { get; set; }
        public DbSet<Categories> categories { get; set; }
        public DbSet<PostJobs> postjobs { get; set; }
        public DbSet<JobOffers> joboffers { get; set; }
        public DbSet<Notification> notification { get; set; }
        public DbSet<SignalRConnectionModel> signalR { get; set; }
        public DbSet<ConfigurationModel> configurations { get; set; }
        public DbSet<PaymentLogModel> paymentLogModels { get; set; }
    }
}
