﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
    public class PushNotificationService
    {
        public string Pushnotification(string deviceid,string message,string body)
        {
            string serverKey = "AAAAiBhV6eA:APA91bEksFRpGxWgk6orchfstm_47yff0e-RPXGIm9kRTN6AvzI7kk3QPQdQWl3RDt3V-b7NznX-5Ee_dznI0hnrqbQkhz-Ywo80no2iZq7oJVbx86DQz8haGYnGxwqrQrQVpTaNuqA3";

            try
            {
                pushdata pushda = new pushdata();
                var result = "-1";
                var webAddr = "https://fcm.googleapis.com/fcm/send";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    pushda.to = deviceid;
                    pushda.notification = new PushNotification();
                    pushda.notification.body = message;
                    pushda.notification.title = "Sweep";
                    pushda.notification.sound = "default";
                    pushda.data = new Data();
                    pushda.data.body = body;
                    string json = JsonConvert.SerializeObject(pushda);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return "success";
            }
            catch (Exception ex)
            {
                return null;
            }
        } 
        public class pushdata
        {
            public PushNotification notification { get; set; }
            public Data data { get; set; }
            public string to { get; set; }
            public string date { get; set; }
        }
        public class PushNotification
        {
            public string body { get; set; }
            public string title { get; set; }
            public string sound { get; set; }

        }
        public class Data
        {
            public string body { get; set; }
            public string title { get; set; }
            public string sound { get; set; }

        }

    }
}
