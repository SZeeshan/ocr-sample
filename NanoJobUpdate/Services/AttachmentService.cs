﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
    public class AttachmentService
    {
        public string UploadAttachmentAsync(byte[] binaryData, string contentType , string AWSKey,string AWSKeySecret,String BucketName)
        {
            var credentials = new BasicAWSCredentials(AWSKey, AWSKeySecret);

            using (IAmazonS3 client = new AmazonS3Client(credentials, RegionEndpoint.CACentral1))
            {
                try
                {
                    // get the file and convert it to the byte[]
                    // create unique file name for prevent the mess
                    var fileName = Guid.NewGuid() + ".png";

                    PutObjectResponse response = null;

                    using (var stream = new MemoryStream(binaryData))
                    {
                        var request = new PutObjectRequest
                        {
                            BucketName = BucketName,
                            Key = fileName,
                            InputStream = stream,
                            ContentType = contentType,
                            CannedACL = S3CannedACL.PublicReadWrite
                        };

                        response =  client.PutObject(request);
                    };
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return fileName;
                    }
                    else
                    {
                        return "invalid";
                    }
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        //Console.WriteLine("Check the provided AWS Credentials.");
                        //Console.WriteLine(
                        //    "For service sign up go to http://aws.amazon.com/s3");
                        return ("Check the provided AWS Credentials.");
                    }
                    else
                    {
                        return String.Format("Error occurred. Message:'{0}' when writing an object", amazonS3Exception.Message);
                    }
                }
            }
        }

        public byte[] GetAttachmentAsync(string attachmentID, string AWSKey, string AWSKeySecret, String BucketName)
        {
            var credentials = new BasicAWSCredentials(AWSKey, AWSKeySecret);

            using (IAmazonS3 client = new AmazonS3Client(credentials, RegionEndpoint.CACentral1))
            {
                try
                {
                    GetObjectRequest request = new GetObjectRequest
                    {
                        BucketName = BucketName,
                        Key = attachmentID,
                    };

                    using (GetObjectResponse response =  client.GetObject(request))
                    using (Stream responseStream = response.ResponseStream)
                    {
                        var bytes = ReadStream(responseStream);
                        return bytes;
                    }
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        return null;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //public bool deleteAttachment(string attachmentID)
        //{
        //    var credentials = new BasicAWSCredentials(ConfigurationManager.AppSettings["AWSKey"].ToString(), ConfigurationManager.AppSettings["AWSKeySecret"].ToString());

        //    using (IAmazonS3 client = new AmazonS3Client(credentials, RegionEndpoint.USEast2))
        //    {
        //        try
        //        {

        //            {
        //                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
        //                {
        //                    BucketName = ConfigurationManager.AppSettings["AWSBucket"].ToString(),
        //                    Key = attachmentID,
        //                };
        //                client.DeleteObject(deleteObjectRequest);
        //            }


        //        }
        //        catch (AmazonS3Exception amazonS3Exception)
        //        {
        //            if (amazonS3Exception.ErrorCode != null &&
        //                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
        //                ||
        //                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
        //            {
        //                return false;

        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        return true;
        //    }
        //}

        public static byte[] ReadStream(Stream responseStream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
