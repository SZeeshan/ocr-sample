﻿using Nanojobs.Infrastructure;
using Nanojobs.Models;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NanojobsDatabase.ApiModel;
using FluentValidation.Results;
using System.Data.Entity;

namespace Nanojobs.Services
{
    public class NotificationService
    {
        public List<notify> GetAllNotification(Guid UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == UserId).FirstOrDefault();
                    if (user.Id != null)
                    {
                        //var res = nanoDbContext.notification.Where(s => s.UserId == user.Id).ToList();
                        List <notify> checknotification = nanoDbContext.notification
                                                                            .Include(s=>s.category)
                                                                            .Where(s=>s.UserId== user.Id)
                                                                             .Select(s => new notify
                                                                             {
                                                                                 Categoryname =s.category.Title,
                                                                                 Status = s.Status,
                                                                                 CategoryId=s.CategoryId

                                                                             }).OrderBy(s=>s.CategoryId).ToList();
                        if (checknotification.Count() != 0)
                        {
                            return checknotification;
                        }
                        return null;
                    }
                    return null;
                }
                
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public class notify
        {
            public string Categoryname { get; set; }
            public bool Status { get; set; }
            public int  CategoryId { get; set; }
        }
        public Notification ChangeNotification(NotificationApiModel notificationapimodel , Guid UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == UserId).FirstOrDefault();
                    if (user != null)
                    {
                        Notification notification = nanoDbContext.notification.Where(s => s.UserId == user.Id && s.CategoryId == notificationapimodel.CategoryId).FirstOrDefault();
                        if (notification != null)
                        {
                            notification.Status = notificationapimodel.Status;
                            nanoDbContext.Entry(notification).State = EntityState.Modified;
                            nanoDbContext.SaveChanges();
                            return notification;
                        }
                        return null;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
               return null;
            }
        }
    }
}