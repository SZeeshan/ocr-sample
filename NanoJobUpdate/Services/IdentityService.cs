﻿using FluentValidation.Results;
using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Nanojobs.ApiModel;
using Nanojobs.Infrastructure;
using Nanojobs.Models;
using NanojobsDatabase.ApiModel;
using NanojobsDatabase.Models;
using NanoJobUpdate.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
    public class IdentityService
    {
        Random random = new Random();
        public class code
        {
            public string Code { get; set; }
        }
        public ServiceResult<code> resendcode(string PhoneNumber)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    DateTime expiryDateTime = DateTime.Now.AddHours(1);
                    UserPhone phone = nanoDbContext.userphone.Where(s => s.PhoneNumber == PhoneNumber).FirstOrDefault();
                    if (phone != null)
                    {
                        code code = new code
                        {
                            Code = random.Next(1000, 9999).ToString()
                        };
                        UserPhone addnewphone = new UserPhone();

                        if (PhoneNumber.Contains("+"))
                        {
                            phone.PhoneNumber = "+" + PhoneNumber.Trim();
                            phone.PhoneNumber = PhoneNumber;
                            phone.Code = code.Code;
                            phone.CreatedOn = DateTime.Now;
                            phone.ExpiryDate = expiryDateTime;
                            nanoDbContext.Entry(phone).State = EntityState.Added;
                            nanoDbContext.SaveChanges();
                            return new ServiceResult<code>(true, null, code);
                        }

                    }
                    else
                    {
                        code code = new code
                        {
                            Code = random.Next(1000, 9999).ToString()
                        };
                        UserPhone addnewphone = new UserPhone();

                        if (!PhoneNumber.Contains("+"))
                        {
                            addnewphone.PhoneNumber = "+" + PhoneNumber.Trim();
                            addnewphone.PhoneNumber = PhoneNumber;
                            addnewphone.Code = code.Code;
                            addnewphone.CreatedOn = DateTime.Now;
                            addnewphone.ExpiryDate = expiryDateTime;
                            nanoDbContext.Entry(addnewphone).State = EntityState.Added;
                            nanoDbContext.SaveChanges();
                        }
                        return new ServiceResult<code>(true, null, code);
                    }
                    return new ServiceResult<code>(false, null, null);
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public ServiceResult<code> addPhonenumber(AddPhoneNumberApiModel addPhone)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    //var plus = "+" + addPhone.PhoneNumber;

                    DateTime expiryDateTime = DateTime.Now.AddHours(1);
                    UserProfile checkprofile = nanoDbContext.userprofile.Where(s => s.PhoneNumber == addPhone.PhoneNumber && s.IsProfile == true).FirstOrDefault();
                    if (checkprofile != null)
                    {
                        return new ServiceResult<code>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", "Number already in use") }, null);
                    }

                    UserPhone phonealreadyexist = nanoDbContext.userphone.Where(s => s.PhoneNumber == addPhone.PhoneNumber).FirstOrDefault();
                    if (phonealreadyexist == null)
                    {
                        code code = new code
                        {
                            Code = random.Next(1000, 9999).ToString()
                        };
                        //string verificationCode = random.Next(1000, 9999).ToString();
                        UserPhone phone = new UserPhone();

                        if (addPhone.PhoneNumber.Contains("+"))
                        {
                            phone.PhoneNumber = addPhone.PhoneNumber;
                            phone.Code = code.Code;
                            phone.CreatedOn = DateTime.Now;
                            phone.ExpiryDate = expiryDateTime;
                            nanoDbContext.Entry(phone).State = EntityState.Added;
                            nanoDbContext.SaveChanges();
                            return new ServiceResult<code>(true, null, code);
                        }
                        return new ServiceResult<code>(true, null, code);
                    }
                }
                return new ServiceResult<code>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", "Phone number already exist") }, null);

            }
            catch (Exception ex)
            {

                return new ServiceResult<code>(false, null, null);
            }

        }
        public ServiceResult<UserProfile> addCardDetail(AddUserCardDetails addcard, Guid userid)
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                try
                {
                    PaypalHelper paypalHelper = new PaypalHelper();
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == userid).FirstOrDefault();
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(addcard.Nonce))
                        {
                            var addtoken = PaypalHelper.AddtokenJobPoster(addcard.Nonce, userid, addcard.PaypalEmail, addcard.CardType, addcard.CardNumber);
                            if (addtoken ==false)
                            {
                                return new ServiceResult<UserProfile>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", "Error") }, null);
                            }
                        }

                        return new ServiceResult<UserProfile>(true, null, null);
                    }
                    return new ServiceResult<UserProfile>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", "Invalid user") }, null);
                }
                catch (Exception ex)
                {

                    return new ServiceResult<UserProfile>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
                }

            }

        }
        public ServiceResult<UserPhone> verifyCode(CodeApiModel codeApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserPhone CodeChecking = nanoDbContext.userphone.Where(s => s.Code == codeApiModel.Code && s.ExpiryDate > DateTime.Now && s.IsValid == false).FirstOrDefault();
                    if (CodeChecking != null)
                    {
                        CodeChecking.IsValid = true;
                        nanoDbContext.Entry(CodeChecking).State = EntityState.Modified;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<UserPhone>(true, null, CodeChecking);

                    }

                    return new ServiceResult<UserPhone>(false, null, null);
                }

            }
            catch (Exception ex)
            {

                return new ServiceResult<UserPhone>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }

        }
        public ServiceResult<string> UpdateLatLong(Guid UserId, ProfileUpdateApiModel profileUpdate)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == UserId).FirstOrDefault();
                    if (user != null)
                    {
                        user.Latitude = profileUpdate.Latitude;
                        user.Longitiude = profileUpdate.Longitude;
                        if (profileUpdate.DeviceId != null)
                        {
                            user.DeviceId = profileUpdate.DeviceId;

                        }
                        nanoDbContext.Entry(user).State = EntityState.Modified;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Profile update successfully");

                    }

                    return new ServiceResult<string>(false, null, null);
                }

            }
            catch (Exception ex)
            {

                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }

        }
        public ServiceResult<string> LogOut(Guid UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == UserId).FirstOrDefault();
                    if (user != null)
                    {
                        PostJobs pendingJob = nanoDbContext.postjobs
                            .Include(s => s.joboffer)
                            .Where(s => (s.UserId == user.Id || s.joboffer.BuyerId == user.Id) && (s.Status == Conts.INPROGRESS || s.Status == Conts.STARTED)).FirstOrDefault();
                        if (pendingJob != null)
                        {
                            return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "You can't logout job is in progress") }, null);
                        }
                        user.DeviceId = null;
                        nanoDbContext.Entry(user).State = EntityState.Modified;
                        nanoDbContext.SaveChanges();
                        nanoDbContext.signalR.RemoveRange(nanoDbContext.signalR.Where(s => s.UserId == UserId));
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Logout successfully");

                    }

                    return new ServiceResult<string>(false, null, null);
                }

            }
            catch (Exception ex)
            {

                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }

        }


        public class customCategoty
        {
            public int Id { get; set; }
        }

        public async Task<ServiceResult<object>> RegisterUser(RegUserApiModel regUserApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    if (!regUserApiModel.PhoneNumber.Contains("+"))
                    {
                        regUserApiModel.PhoneNumber = "+" + regUserApiModel.PhoneNumber.Trim();
                    }

                    //ChatHub.Server_Send("JobSearch", "sadsad");

                    UserProfile userProfile = nanoDbContext.userprofile.Where(s => s.Email == regUserApiModel.Email).FirstOrDefault();

                    if (userProfile == null)
                    {
                        var user = new UserProfile()
                        {
                            Id = Guid.NewGuid(),
                            Email = regUserApiModel.Email,
                            PhoneNumber = regUserApiModel.PhoneNumber,
                            FirstName = regUserApiModel.FirstName,
                            LastName = regUserApiModel.LastName,
                            CreatedOn = DateTime.Now,
                            Location = regUserApiModel.Location,
                            UserName = regUserApiModel.Email,
                            IsVerified = true,
                            Latitude = regUserApiModel.Latitude,
                            Longitiude = regUserApiModel.Longitude,
                            PasswordHash = PasswordService.MD5Hash(regUserApiModel.Password),
                            IsProfile = true,
                            Balance = 0,
                            IsValidPaypal = false
                        };
                        //IdentityResult addUserResult = await SignInManager.UserManager.CreateAsync(user, regUserApiModel.Password);
                        nanoDbContext.userprofile.Add(user);
                        nanoDbContext.SaveChanges();

                        List<customCategoty> categories = nanoDbContext.categories.Select(s => new customCategoty() { Id = s.Id }).ToList();
                        foreach (customCategoty category in categories)
                        {
                            Notification add = new Notification();
                            add.ID = Guid.NewGuid();
                            add.UserId = user.Id;
                            add.CategoryId = category.Id;
                            add.Status = true;
                            nanoDbContext.notification.Add(add);
                            nanoDbContext.SaveChanges();
                        }

                        var identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

                        identity.AddClaim(new Claim(ClaimTypes.Name, user.Id.ToString()));
                        identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));

                        var props = new AuthenticationProperties(new Dictionary<string, string>
                        {

                        });

                        var ticket = new AuthenticationTicket(identity, props);

                        ticket.Properties.IssuedUtc = DateTime.UtcNow;
                        ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddYears(3);

                        var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
                        return new ServiceResult<object>(true, null, new { access_token = accessToken, token_type = "bearer", expires_in = DateTime.UtcNow.AddYears(3), user = user });
                    }
                }

                return new ServiceResult<object>(false, new List<ValidationFailure> { new ValidationFailure("identity", "Email already exist") }, null);
            }
            catch (Exception ex)
            {
                return new ServiceResult<object>(false, new List<ValidationFailure> { new ValidationFailure("identity", ex.Message) }, null);
            }
        }

        public async Task<ResponseModel> LoginUser(LoginApiModel loginApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Email == loginApiModel.Email).FirstOrDefault();

                    if (user == null)
                    {
                        return SuccessReturnModel.FailureModel("The email or password is incorrect!");
                    }

                    string pass = PasswordService.MD5Hash(loginApiModel.Password);

                    UserProfile userprofile = nanoDbContext.userprofile.Where(s => s.Email == loginApiModel.Email && s.PasswordHash == pass).FirstOrDefault();
                    if (userprofile == null)
                    {
                        return SuccessReturnModel.FailureModel("The email or password is incorrect!");
                    }

                    var identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

                    identity.AddClaim(new Claim(ClaimTypes.Name, userprofile.Id.ToString()));
                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userprofile.Id.ToString()));

                    var props = new AuthenticationProperties(new Dictionary<string, string>
                    {

                    });

                    var ticket = new AuthenticationTicket(identity, props);

                    ticket.Properties.IssuedUtc = DateTime.UtcNow;
                    ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddYears(3);

                    var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
                    // var aa =await ChatHub.OnConnected();

                    //Guid guid = user.Id;

                    //SignalRConnectionModel signalRConnectionModel = new SignalRConnectionModel()
                    //{
                    //    ConnectionId = guid.ToString(),
                    //    IsConnected = true,
                    //    UserId = guid,
                    //    UserAgent = "iOS",
                    //};

                    //nanoDbContext.signalR.Add(signalRConnectionModel);
                    //nanoDbContext.SaveChanges();

                    return SuccessReturnModel.SuccessModel(new { access_token = accessToken, token_type = "bearer", expires_in = DateTime.UtcNow.AddYears(3), user = user }, "Login Successfully");
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ServiceResult<bool> ChangePassword(string UserId, ChangePasswordApiModel changePasswordApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    Guid userGuid = new Guid(UserId);
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == userGuid).FirstOrDefault();

                    if (user == null)
                        return new ServiceResult<bool>(false, new List<ValidationFailure> { new ValidationFailure("error", "User Not Found") }, false);
                    var password = PasswordService.MD5Hash(changePasswordApiModel.OldPassword);
                    if (user.PasswordHash == password)
                    {
                        user.PasswordHash = PasswordService.MD5Hash(changePasswordApiModel.NewPassword);
                        nanoDbContext.Entry(user).State = EntityState.Modified;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<bool>(true, null, true);

                    }
                    return new ServiceResult<bool>(false, new List<ValidationFailure> { new ValidationFailure("error", "Incorrect Password") }, false);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<bool>(false, new List<ValidationFailure> { new ValidationFailure("error", ex.Message) }, false);
            }
        }

        public ServiceResult<string> ForgotPassword(ForgotPasswordApiModel forgotPasswordApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Email == forgotPasswordApiModel.email).FirstOrDefault();

                    if (user == null)
                        return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", "Email Does Not Exist!") }, null);

                    string password = Path.GetRandomFileName();
                    password = password.Replace(".", "");

                    user.PasswordHash = PasswordService.MD5Hash(password);
                    nanoDbContext.Entry(user).State = EntityState.Modified;
                    nanoDbContext.SaveChanges();

                    var FromEmail = ConfigurationManager.AppSettings["email"];
                    EmailService.SendEmail("Password Reset Email", "Here is the random password as per your request you can login through this password" + " " + password, FromEmail, user.Email, null);
                    return new ServiceResult<string>(true, null, null);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", ex.Message) }, null);
            }
        }

        public ServiceResult<UserReturnModel> GetCurrentUser(string userId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    Guid userGuid = new Guid(userId);
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == userGuid).FirstOrDefault();

                    if (user == null)
                        return new ServiceResult<UserReturnModel>(false, new List<ValidationFailure> { new ValidationFailure("error", "Unauthorized") }, null);

                    UserReturnModel usermodel = new UserReturnModel();
                    usermodel.Email = user.Email;
                    usermodel.PhoneNumber = user.PhoneNumber;
                    usermodel.FirstName = user.FirstName;
                    usermodel.LastName = user.LastName;
                    usermodel.DeviceId = user.DeviceId;
                    usermodel.Id = user.Id;
                    usermodel.Latitude = user.Latitude;
                    usermodel.IsVerified = user.IsVerified;
                    usermodel.Location = user.Location;
                    usermodel.Longitiude = user.Longitiude;
                    usermodel.PhoneNumber = user.PhoneNumber;
                    usermodel.Balance = user.Balance;
                    usermodel.IsValidPaypal = user.IsValidPaypal;
                    usermodel.PaypalEmailAddress = user.PaypalEmail;
                    if (user.ProfilePicture != null)
                    {
                        // AttachmentService attachment = new AttachmentService();
                        //var aa = attachment.GetAttachmentAsync(user.ProfilePicture, ConfigurationManager.AppSettings["Awskey"], ConfigurationManager.AppSettings["AwsSecretKey"], ConfigurationManager.AppSettings["AwsBucketName"]);
                        usermodel.ProfilePicture = user.ProfilePicture;
                    }
                    usermodel.UserName = user.UserName;
                    List<UserCardDetails> carddetails = nanoDbContext.usercarddetails.Where(s => s.UserId == user.Id.ToString()).ToList();
                    if (carddetails.Count() != 0)
                    {
                        usermodel.Usercarddetails = new List<UserCarddetailsModel>();
                        for (int i = 0; i < carddetails.Count(); i++)
                        {

                            usermodel.Usercarddetails.Add(new UserCarddetailsModel
                            {
                                CardNumber = carddetails[i].CardNumber,
                                CardType = carddetails[i].CardType,
                                Id = carddetails[i].Id,
                                IsDefault = carddetails[i].IsDefault,
                                PaypalEmail = carddetails[i].PayPalEmal
                            });
                        }

                    }

                    return new ServiceResult<UserReturnModel>(true, null, usermodel);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<UserReturnModel>(false, new List<ValidationFailure> { new ValidationFailure("error", ex.Message) }, null);
            }
        }
        public ServiceResult<string> SetDefault(int Id, string userId)
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                Guid userGuid = new Guid(userId);
               List <UserCardDetails> Card = nanoDbContext.usercarddetails.Where(s => s.UserId == userGuid.ToString()).ToList();
                for (int i = 0; i < Card.Count(); i++)
                {
                    Card[i].IsDefault = false;
                    nanoDbContext.Entry(Card).State = EntityState.Modified;
                    nanoDbContext.SaveChanges();
                }
                UserCardDetails userCard = nanoDbContext.usercarddetails.Where(s => s.UserId == userGuid.ToString() && s.Id==Id).FirstOrDefault();
                if (userCard !=null)
                {
                    userCard.IsDefault = true;
                    nanoDbContext.Entry(userCard).State = EntityState.Modified;
                    nanoDbContext.SaveChanges();
                    return new ServiceResult<string>(true, null, "Default payment ");
                }
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", "No record") }, null);
            }
        }

        public ServiceResult<string> EditProfile(EditProfileApiModel editProfileApiModel, string userId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    Guid userGuid = new Guid(userId);
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == userGuid).FirstOrDefault();

                    if (user == null)
                        return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", "Unauthorized") }, null);

                    if (user != null)
                    {
                        if (!editProfileApiModel.ProfilePicture.Contains("https://s3.ca-central-1.amazonaws.com/nanojobs/ProfilePicture"))
                        {
                            var array = Convert.FromBase64String(editProfileApiModel.ProfilePicture);
                            if (editProfileApiModel.ProfilePicture != null)
                            {
                                AttachmentService attachment = new AttachmentService();
                                user.ProfilePicture = "https://s3.ca-central-1.amazonaws.com/nanojobs/ProfilePicture/" + attachment.UploadAttachmentAsync(array, "image/png", ConfigurationManager.AppSettings["Awskey"], ConfigurationManager.AppSettings["AwsSecretKey"], ConfigurationManager.AppSettings["AwsBucketName"]);
                            }
                        }

                        user.FirstName = editProfileApiModel.FirstName;
                        user.LastName = editProfileApiModel.LastName;
                        user.Location = editProfileApiModel.Location;
                        user.Longitiude = editProfileApiModel.Longitude;
                        user.Latitude = editProfileApiModel.Latitude;
                        nanoDbContext.Entry(user).State = EntityState.Modified;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Profile edit successfully");
                    }

                }
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", "No record") }, null);
            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("error", ex.Message) }, null);
            }
        }
        public ServiceResult<EditProfileApiModel> GetProfile(string userId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    EditProfileApiModel editProfileApiModel = new EditProfileApiModel();
                    Guid userGuid = new Guid(userId);
                    UserProfile user = nanoDbContext.userprofile.Where(s => s.Id == userGuid).FirstOrDefault();

                    if (user == null)
                        return new ServiceResult<EditProfileApiModel>(false, new List<ValidationFailure> { new ValidationFailure("error", "Unauthorized") }, null);

                    if (user != null)
                    {
                        if (user.ProfilePicture != null)
                        {
                            // AttachmentService attachment = new AttachmentService();
                            //var aa = attachment.GetAttachmentAsync(user.ProfilePicture, ConfigurationManager.AppSettings["Awskey"], ConfigurationManager.AppSettings["AwsSecretKey"], ConfigurationManager.AppSettings["AwsBucketName"]);
                            editProfileApiModel.ProfilePicture = user.ProfilePicture;
                        }
                        editProfileApiModel.Location = user.Location;
                        editProfileApiModel.FirstName = user.FirstName;
                        editProfileApiModel.LastName = user.LastName;
                        return new ServiceResult<EditProfileApiModel>(true, null, editProfileApiModel);
                    }

                }
                return new ServiceResult<EditProfileApiModel>(false, new List<ValidationFailure> { new ValidationFailure("error", "No record") }, null);
            }
            catch (Exception ex)
            {
                return new ServiceResult<EditProfileApiModel>(false, new List<ValidationFailure> { new ValidationFailure("error", ex.Message) }, null);
            }
        }
    }
}
