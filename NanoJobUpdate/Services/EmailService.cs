﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
        public static class EmailService
        {
            public static bool SendEmail ( string subject, string body, string From, string To, string Bcc)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient client = new SmtpClient();
                    client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                    client.Timeout = 10000;
                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(From, ConfigurationManager.AppSettings["password"]);
                    client.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                    mail.Subject = subject;
                    mail.IsBodyHtml = true;
                    mail.Body = body;
                    mail.From = new MailAddress(From);
                    if (Bcc != null)
                    {
                        mail.Bcc.Add(Bcc);
                    }

                    mail.To.Add(To);
                client.Send(mail);

                return true;
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                    return false;
                }
            }
        }
}
