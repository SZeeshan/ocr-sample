﻿using FluentValidation.Results;
using Nanojobs.Infrastructure;
using Nanojobs.Models;
using NanojobsDatabase.ApiModel;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NanoJobUpdate.Services
{
    public class OcrService
    {
        public ServiceResult<string> RegisterUser(UserApiModel userApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserDetails user = nanoDbContext.userDetails.Where(s => s.Email == userApiModel.Email).FirstOrDefault();
                    if (user == null)
                    {
                        UserDetails users = new UserDetails();
                        users.Email = userApiModel.Email;
                        users.Password = userApiModel.Password;
                        users.CreatedOn = DateTime.UtcNow;
                        nanoDbContext.Entry(users).State = EntityState.Added;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "User register successfully");

                    }

                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "Email already exist") }, null);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }
        }
        public ServiceResult<UserDetails> Login(UserApiModel userApiModel)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserDetails user = nanoDbContext.userDetails.Where(s => s.Email == userApiModel.Email && s.Password == userApiModel.Password).FirstOrDefault();
                    if (user != null)
                    {
                        return new ServiceResult<UserDetails>(true, null, user);
                    }

                    return new ServiceResult<UserDetails>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "Some of your information may wrong") }, null);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<UserDetails>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }
        }
        public ServiceResult<string> AddCustomPerfence(string Ingredient, int UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserPreference userPreference = nanoDbContext.userPreferences.Where(s => s.CustomValues == Ingredient && s.UserId == UserId).FirstOrDefault();
                    if (userPreference == null)
                    {
                        UserPreference user = new UserPreference();
                        user.CustomValues = Ingredient;
                        user.UserId = UserId;
                        nanoDbContext.Entry(user).State = EntityState.Added;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Add Custom Perefence successfully");
                    }

                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "Already added") }, null);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }
        }
        public ServiceResult<string> AddAppCategory(int CategoryId, int UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    UserCategory userPreference = nanoDbContext.userCategories.Where(s => s.CategoryId == CategoryId && s.UserId == UserId).FirstOrDefault();
                    if (userPreference == null)
                    {
                        UserCategory user = new UserCategory();
                        user.CategoryId = CategoryId;
                        user.UserId = UserId;
                        nanoDbContext.Entry(user).State = EntityState.Added;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Add App Perefence successfully");
                    }

                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "Already added") }, null);
                }

            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Code", ex.Message) }, null);
            }
        }
        public ServiceResult<List<UserPreference>> GetAllCustomPerfence(int UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    List<UserPreference> userPreference = nanoDbContext.userPreferences.Where(s => s.UserId == UserId).ToList();
                    if (userPreference.Count() != 0)
                    {
                        return new ServiceResult<List<UserPreference>>(true, null, userPreference);
                    }

                    return new ServiceResult<List<UserPreference>>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<List<UserPreference>>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public ServiceResult<List<Category>> GetAllCategory()
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    List<Category> userPreference = nanoDbContext.getCategories.ToList();
                    if (userPreference.Count() != 0)
                    {
                        return new ServiceResult<List<Category>>(true, null, userPreference);
                    }

                    return new ServiceResult<List<Category>>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<List<Category>>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public ServiceResult<string> deleteCatbyId(int Id, int UserId)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    if (Id!=0)
                    {
                        UserPreference userPreference = nanoDbContext.userPreferences.Where(s => s.Id == Id && s.UserId == UserId).FirstOrDefault();
                        if (userPreference !=null)
                        {
                            nanoDbContext.userPreferences.Remove(userPreference);
                            nanoDbContext.SaveChanges();
                        }
                        UserCategory userCategory = nanoDbContext.userCategories.Where(s => s.Id == Id && s.UserId == UserId).FirstOrDefault();
                        if (userCategory !=null)
                        {
                            nanoDbContext.userCategories.Remove(userCategory);
                            nanoDbContext.SaveChanges();
                        }
                        return new ServiceResult<string>(true, null, "delete successfully");
                    }
                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public ServiceResult<string> UpdatebyId(int Id, int UserId,string Value)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    if (Id != 0)
                    {
                        UserPreference userPreference = nanoDbContext.userPreferences.Where(s => s.Id == Id && s.UserId == UserId).FirstOrDefault();
                        if (userPreference != null)
                        {
                            userPreference.CustomValues = Value;
                            nanoDbContext.Entry(userPreference).State = EntityState.Modified;
                            nanoDbContext.SaveChanges();
                        }
                        
                        return new ServiceResult<string>(true, null, "Update successfully");
                    }
                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public ServiceResult<getalllist> AllAppCategory(int Userid)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    getalllist all = new getalllist();
                    var aa = nanoDbContext.userPreferences.Where(s => s.UserId == Userid).ToList();
                    var bb = nanoDbContext.userCategories.Include(s => s.dbCategory)
                        .Where(s => s.UserId == Userid).ToList();
                    if (Userid != 0)
                    {
                        
                        all.AppCustomPrefenceList = new List<getcustomlist>();
                        all.AppPrefenceList = new List<getallapplist>();

                        for (int i = 0; i < aa.Count(); i++)
                        {
                           
                            getcustomlist getcustomlist = new getcustomlist();
                            getcustomlist.Id = aa[i].Id;
                            getcustomlist.Value = aa[i].CustomValues;
                           
                            all.AppCustomPrefenceList.Add(getcustomlist);
                        }

                        for (int j = 0; j < bb.Count(); j++)
                        {
                            getallapplist list = new getallapplist();
                            list.Id = bb[j].Id;
                            list.Value = bb[j].dbCategory.CategoryValues;
                            list.CategoryName = bb[j].dbCategory.CategoriesName;
                           

                            all.AppPrefenceList.Add(list);
                        }
                       
                        return new ServiceResult<getalllist>(true, null, all);
                    }

                    return new ServiceResult<getalllist>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<getalllist>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public ServiceResult<string> AddCategory(Categories categoryName)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    if (categoryName.CategoryName != null)
                    {
                        Category category = new Category();
                        category.CategoriesName = categoryName.CategoryName;
                        category.CategoryValues = categoryName.Value;
                        nanoDbContext.Entry(category).State = EntityState.Added;
                        nanoDbContext.SaveChanges();
                        return new ServiceResult<string>(true, null, "Added");
                    }

                    return new ServiceResult<string>(false, new List<FluentValidation.Results.ValidationFailure> { new ValidationFailure("error", "No records") }, null);
                }
            }
            catch (Exception ex)
            {
                return new ServiceResult<string>(false, new List<ValidationFailure> { new ValidationFailure("Invalid Request", ex.Message) }, null);
            }
        }
        public class getalllist
        {
            public List<getcustomlist> AppCustomPrefenceList { get; set; }
            public List<getallapplist> AppPrefenceList { get; set; }
        }
        public class getcustomlist
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }
        public class getallapplist
        {
            public int Id { get; set; }
            public string Value { get; set; }
            public string CategoryName { get; set; }
        }
            
            public class Categories
            {
                public string CategoryName { get; set; }
                public string Value { get; set; }
            }
            public class UserApiModel
            {
                public string Email { get; set; }
                public string Password { get; set; }
            }
        }
    }
