﻿namespace Nanojobs.Services
{
    internal class UploadPhotoModel
    {
        public bool Success { get; set; }
        public string FileName { get; set; }
    }
}