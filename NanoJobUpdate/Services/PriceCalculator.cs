﻿using Nanojobs.Infrastructure;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
    public class PriceCalculator
    {
        public double PriceCalc(DateTime JobStartTime, DateTime JobCompletedTime, int Price)
        {
            try
            {
                if (JobCompletedTime !=null && JobStartTime !=null)
                {
                    double time = JobCompletedTime.Subtract(JobStartTime).TotalMinutes;
                    if (time <= 120)
                    {
                        double aa = Price;
                        return aa;
                    }
                    else
                    {
                        double fifteenrate = Convert.ToDouble(Price)/8 ;
                        double amount = Price;
                        double remainingtime = ((int)time - 120);
                        int fraction = (int)Math.Ceiling(remainingtime / 15);
                        amount = amount + (fraction * fifteenrate);
                        return amount;
                        // var aa = 0;
                    }
                }
                return 0;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public durationmodel TotalDuration(DateTime JobStartTime, DateTime JobCompletedTime)
        {
            try
            {
                if (JobCompletedTime != null && JobStartTime != null)
                {
                    var time = JobCompletedTime.Subtract(JobStartTime);
                    durationmodel duration = new durationmodel();
                    duration.Hours = time.Hours;
                    duration.Mins = time.Minutes;
                    duration.Sec = time.Seconds;
                    return duration;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public class durationmodel
        {
            public int? Hours { get; set; }
            public int? Mins { get; set; }
            public int? Sec { get; set; }
        }
        public double JobPosterPrice(double amount )
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    ConfigurationModel markup = nanoDbContext.configurations.Where(s => s.Tittle == "JobPoster").FirstOrDefault();
                    double percentagemarkup = ((double)amount) / 100 * markup.CompanyFees;
                    double totalprice = (percentagemarkup + amount);
                    return totalprice;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        public double JobPosterCommision(double amount)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    ConfigurationModel markup = nanoDbContext.configurations.Where(s => s.Tittle == "JobPoster").FirstOrDefault();
                    double percentagemarkup = ((double)amount) / 100 * markup.CompanyFees;
                    return percentagemarkup;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public double JobAcceptCommision(double amount)
        {
            try
            {
                using (NanoDbContext nanoDbContext = new NanoDbContext())
                {
                    ConfigurationModel markup = nanoDbContext.configurations.Where(s => s.Tittle == "JobAccept").FirstOrDefault();
                    double percentagemarkup = ((double)amount) / 100 * markup.CompanyFees;
                    return percentagemarkup;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
