﻿using Nanojobs.Infrastructure;
using Nanojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Services
{
    public class CategoryServices
    {

        public List<Categories> GetCategories()
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                var res = nanoDbContext.categories.ToList();
                return res;
            }
            
        }
    }
}
