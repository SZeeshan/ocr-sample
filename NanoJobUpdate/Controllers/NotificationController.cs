﻿using Nanojobs.ApiModel;
using Nanojobs.Infrastructure;
using Nanojobs.Services;
using NanojobsDatabase.ApiModel;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Nanojobs.Controllers
{
    [RoutePrefix("api/notification")]

    public class NotificationController : ApiController
    {
        [Route("getallnotification")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllNotification()
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                NotificationService service = new NotificationService();
                var result = service.GetAllNotification((guid));

                if (result !=null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result, "User Notification"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("No records"));
        }


        [Route("changenotification")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Notification([FromBody]NotificationApiModel notificationapimodel)
        {
            Notification result = null;
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                NotificationService service = new NotificationService();
                result =  service.ChangeNotification(notificationapimodel, guid);

                if (result!=null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result, "Notifications changed sucessfully"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Notification failed to change"));
        }
    }

}


