﻿using Nanojobs.ApiModel;
using Nanojobs.Infrastructure;
using Nanojobs.Models;
using Nanojobs.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nanojobs.Controllers
{
    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {
        [Route("getallservice")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll()
        {
            CategoryServices service = new CategoryServices();
            List<Categories> result = service.GetCategories();
            return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result, "Mass Services"));
        }
    }
}