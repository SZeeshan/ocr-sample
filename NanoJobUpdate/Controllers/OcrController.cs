﻿using Nanojobs.ApiModel;
using NanoJobUpdate.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static NanoJobUpdate.Services.OcrService;

namespace NanoJobUpdate.Controllers
{
    [RoutePrefix("api/Ocr")]
    public class OcrController : ApiController
    {
        [Route("Login")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Login([FromBody] UserApiModel userApiModel)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.Login(userApiModel);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Login successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("Register")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Register([FromBody] UserApiModel userApiModel)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.RegisterUser(userApiModel);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Register successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("AddCustomPreference")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage AddCustomPreference(string Ingredient, int UserId)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.AddCustomPerfence(Ingredient, UserId);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Added prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("AddAppPreference")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage AddCustomPerfence(int CategoryId, int UserId)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.AddAppCategory(CategoryId, UserId);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Added prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("GetAllCustomPerfence")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAllCustomPerfence(int UserId)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.GetAllCustomPerfence(UserId);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get all prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("AllAppCategory")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage AllAppCategory(int UserId)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.AllAppCategory(UserId);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get all prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("DeletePerfence")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage Perfence(int Id,int UserId)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.deleteCatbyId(Id, UserId);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Delete prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("UpdatePerfence")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage UpdatePerfence(int Id, int UserId ,string Value)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.UpdatebyId(Id, UserId, Value);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Delete prefence successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("GetAllCategory")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAllCategory()
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.GetAllCategory();

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get all category successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("AddCategory")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage AddCategory(Categories categories)
        {
            if (ModelState.IsValid)
            {
                OcrService service = new OcrService();
                var result = service.AddCategory(categories);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get all category successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
    }
}
