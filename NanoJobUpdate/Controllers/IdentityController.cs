﻿using Microsoft.AspNet.Identity;
using Nanojobs.ApiModel;
using Nanojobs.Infrastructure;
using Nanojobs.Models;
using Nanojobs.Services;
using NanojobsDatabase.ApiModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Nanojobs.Controllers
{
    [RoutePrefix("api/identity")]
    public class IdentityController : ApiController
    {

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Register([FromBody] RegUserApiModel regUserApiModel)
        {
            if (ModelState.IsValid)
            {
               IdentityService service = new IdentityService();
               var result = await service.RegisterUser(regUserApiModel);

                if (result.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Register successfully"));
                }

                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("identity", item.ErrorMessage);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("ERROR"));
        }

        [Route("manual")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult ManualTrigger()
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                var job = nanoDbContext.postjobs.FirstOrDefault();

                socketModel sm = new socketModel();
                sm.CHANNEL = "JobSearch";
                sm.DATA = job;
                string serializedData = Newtonsoft.Json.JsonConvert.SerializeObject(sm);
                ChatHub.Server_Send(serializedData);
                return Ok();
            }
               
        }

        public class socketModel
        {
            public dynamic DATA { get; set; }
            public string CHANNEL { get; set; }
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Login([FromBody] LoginApiModel loginApiModel)
        {
            if (ModelState.IsValid)
            {
                IdentityService service = new IdentityService();
                var result = await service.LoginUser(loginApiModel);

                if (result.success)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("ERROR"));
        }
        [Route("addphone")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage addphone([FromBody] AddPhoneNumberApiModel addPhone)
        {
            if (ModelState.IsValid)
            {
                IdentityService service = new IdentityService();
                var result = service.addPhonenumber(addPhone);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Phone number added successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("addcarddetails")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage addcarddetails(AddUserCardDetails addUser)
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                IdentityService service = new IdentityService();
                var result = service.addCardDetail(addUser, guid);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Card details added successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("SetDefault")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage SetDefault(int Id)
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                IdentityService service = new IdentityService();
                var result = service.SetDefault(Id, user);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Default card set"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Request"));
        }
        [Route("resendcode")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage resendcode(string PhoneNumber)
        {
            if (ModelState.IsValid)
            {
                IdentityService service = new IdentityService();
                var result = service.resendcode(PhoneNumber);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Phone number resend successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Already send"));
        }
        [Route("verifycode")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage verifycode([FromBody] CodeApiModel codeApiModel )
        {
            if (ModelState.IsValid)
            {
                IdentityService service = new IdentityService();
                var result =  service.verifyCode(codeApiModel);

                if (result !=null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Code verified"));
                }

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid Code"));
        }
        [Route("updatelatlong")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage updatelatlong([FromBody] ProfileUpdateApiModel profileUpdate)
        {
           
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                IdentityService service = new IdentityService();
                var result =  service.UpdateLatLong(guid, profileUpdate);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(null, "Profile update successfully"));
                }

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid request"));
        }
        [Route("logout")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage logout()
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;
                Guid guid = new Guid(user);
                IdentityService service = new IdentityService();
                var result = service.LogOut(guid);

                if (result.IsValid == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(null, "Logout successfully"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid request"));
        }

        [Route("changepassword")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ChangePassword([FromBody] ChangePasswordApiModel changePasswordApiModel)
        {
            if (ModelState.IsValid)
            {
                string userid = User.Identity.Name;

                IdentityService service = new IdentityService();
                ServiceResult<bool> result =  service.ChangePassword(userid, changePasswordApiModel);

                if (result.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(null, "Password Changed Successfully"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Some of your information may wrong"));
        }

        [HttpPost]
        [Route("forgotpassword")]
        public HttpResponseMessage ForgotPassword([FromBody] ForgotPasswordApiModel forgotPasswordApiModel)
        {
            if (ModelState.IsValid)
            {
                IdentityService service = new IdentityService();
                ServiceResult<string> result = service.ForgotPassword(forgotPasswordApiModel);

                if (result.IsValid ==true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(null, "An email has been sent to you with password reset instructions"));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel(result.Errors[0].ErrorMessage));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Some of your information may wrong"));
    }
        [Route("currentuser")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetCurrentUser()
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;

                IdentityService service = new IdentityService();
                ServiceResult<UserReturnModel> result =service.GetCurrentUser(user);

                if (result.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get currenct user"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid request"));
        }
        [Route("editprofile")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage EditProfile([FromBody]EditProfileApiModel editProfileApiModel)
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;

                IdentityService service = new IdentityService();
                ServiceResult<string> result = service.EditProfile(editProfileApiModel, user);

                if (result.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Profile edit successfully"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid request"));
        }
        [Route("getprofile")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetProfile()
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.Name;

                IdentityService service = new IdentityService();
                ServiceResult<EditProfileApiModel> result =  service.GetProfile(user);

                if (result.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SuccessReturnModel.SuccessModel(result.Value, "Get Profile successfully"));
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, SuccessReturnModel.FailureModel("Invalid request"));
        }

    }
}
