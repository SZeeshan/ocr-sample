﻿using Nanojobs.Infrastructure;
using NanojobsDatabase.ApiModel;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Nanojobs.Repositories
{
    public class JobRepository
    {
        NanoDbContext nanoDbContext;

        public JobRepository()
        {
            nanoDbContext = new NanoDbContext();
        }

        public List<SearchReturnModel> GetJobsSearch()
        {
            DataSet ds = new DataSet();
            string connection = ConfigurationManager.ConnectionStrings["defaultconnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connection))
            {
               
                SqlCommand cmd = new SqlCommand("SpJobSearch", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
                mySqlDataAdapter.SelectCommand = cmd;
                cmd.CommandTimeout = Convert.ToInt32(300);

                mySqlDataAdapter.Fill(ds);
            }
            if (ds == null || ds.Tables.Count == 0) return null;
            DataTable dtData = ds.Tables[0];

            var list = dtData.AsEnumerable().Select(s => new SearchReturnModel()
            {
                Price = s.Field<int?>("Wage"),
                CategoryId = s.Field<int>("CategoryId"),
                CategoryName = s.Field<string>("Title"),
                Description = s.Field<string>("Description"),
                Latitude = s.Field<string>("Latitude"),
                Longitude = s.Field<string>("Longitude"),
                Link = s.Field<string>("JobSearchLink"),
                Location = s.Field<string>("Location"),
                PostalCode = s.Field<string>("PostalCode"),
                ProfilePicture = s.Field<string>("ProfilePicture"),
                UserName = s.Field<string>("FirstName")+" "+ s.Field<string>("LastName"),
                Id = s.Field<Guid>("Id"),
                PopUpLink = s.Field<string>("PopUpLink"),
                UserId = s.Field<Guid>("UserId"),
            }).ToList();

            return list;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    nanoDbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
