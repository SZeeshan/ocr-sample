﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoJobUpdate.CronJobs
{
    public class JobScheduler
    {
        public  static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail sellerjobs = JobBuilder.Create<SearchJobs>().Build();

            ITrigger sellertrigger = TriggerBuilder.Create().WithIdentity("NewJob")
                .StartNow()
                .WithSimpleSchedule
                  (s => s
                    .WithIntervalInSeconds(5)
                    .RepeatForever()
                  )
                .Build();

            scheduler.ScheduleJob(sellerjobs, sellertrigger);
        }
    }
}