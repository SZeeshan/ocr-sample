﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace NanoJobUpdate.CronJobs
{
    [DisallowConcurrentExecution]
    public class SearchJobs : IJob
    {
        SearchJobService searchJobService { get; set; }

        public SearchJobs()
        {
            searchJobService = new SearchJobService();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                searchJobService.SearchNewJobs();
            }
            catch (Exception ex)
            {
                // log.Error("Validic Polling failure", ex);
            }

            //log.Info("Executing Validic Polling Job - Finished");
        }

    }
}