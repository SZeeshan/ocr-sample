﻿using Nanojobs.Infrastructure;
using NanojobsDatabase.ApiModel;
using NanojobsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace NanoJobUpdate.CronJobs
{
    public class SearchJobService
    {
        public  void SearchNewJobs()
        {
            using (NanoDbContext db = new NanoDbContext())
            {
                var subtractime = DateTime.Now.AddSeconds(-5);


                List<PostJobs> job = db.postjobs
                    .Include(s=>s.joboffer.buyername)
                    .Where(s => (s.Status == Conts.INPROGRESS || s.Status == Conts.STARTED))
                    .ToList();
                SocketReturnModel socketReturn = new SocketReturnModel();
                socketReturn.CHANNEL = "UpdateLatLong";
                socketReturn.DATA = job;
                string serializedData = Newtonsoft.Json.JsonConvert.SerializeObject(job);
                ChatHub.Server_Send(serializedData);
            }
        }
    }
}