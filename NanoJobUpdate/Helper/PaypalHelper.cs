﻿using Braintree;
using Nanojobs.Infrastructure;
using Nanojobs.Models;
using NanojobsDatabase.Models;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NanoJobUpdate.Helper
{
    public class PaypalHelper
    {
        public bool DeductpaymentFromJobPoster(string Token, string CustomerId, double amount,string type)
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                var gateway = new BraintreeGateway
                {
                    //global env invision sqa
                    Environment = Braintree.Environment.SANDBOX,
                    MerchantId = "gn2vzmpk7ztqzcmt",
                    PublicKey = "qnwpg7f22zcj3w73",
                    PrivateKey = "93678df3e0322625e780650a30435e00"
                };
                if (type=="PayPal")
                {
                    var requestss = new TransactionRequest
                    {
                        Amount = (decimal)amount,
                        //for credit card merchantid not incld
                        MerchantAccountId = "Canadian",
                        PaymentMethodToken = Token,
                        CustomerId = CustomerId,

                        Options = new TransactionOptionsRequest
                        {
                            SubmitForSettlement = true,
                            StoreInVaultOnSuccess = true
                        }
                    };
                    Result<Braintree.Transaction> result = gateway.Transaction.Sale(requestss);
                    if (result.IsSuccess() == true)
                    {
                        return true;
                    }
                }
                else
                {
                    var requestss = new TransactionRequest
                    {
                        Amount = (decimal)amount,
                        //for credit card merchantid not incld
                        //MerchantAccountId = "Canadian",
                        PaymentMethodToken = Token,
                        CustomerId = CustomerId,

                        Options = new TransactionOptionsRequest
                        {
                            SubmitForSettlement = true,
                            StoreInVaultOnSuccess = true
                        }
                    };
                    Result<Braintree.Transaction> result = gateway.Transaction.Sale(requestss);
                    if (result.IsSuccess() == true)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        public static bool AddtokenJobPoster(string Token, Guid Userid,string PaypalEmail, string CardType, string CardNumber)
        {
            using (NanoDbContext nanoDbContext = new NanoDbContext())
            {
                try
                {
                    var gateway = new BraintreeGateway
                    {
                        //global env invision sqa
                        Environment = Braintree.Environment.SANDBOX,
                        MerchantId = "gn2vzmpk7ztqzcmt",
                        PublicKey = "qnwpg7f22zcj3w73",
                        PrivateKey = "93678df3e0322625e780650a30435e00"

                    };
                    UserProfile user = nanoDbContext.userprofile
                          .Where(s => s.Id == Userid)
                          .FirstOrDefault();
                    if (user != null)
                    {
                        var request = new CustomerRequest
                        {
                            //email address for new customer 
                            Email = PaypalEmail
                        };
                        Result<Customer> result = gateway.Customer.Create(request);
                        bool success = result.IsSuccess();
                        if (success == true)
                        {
                            //make customer 
                            string customerId = result.Target.Id;
                            var requestss = new PaymentMethodRequest
                            {
                                CustomerId = customerId,
                                PaymentMethodNonce = Token,
                            };
                            Result<PaymentMethod> results = gateway.PaymentMethod.Create(requestss);
                            //var apiContext = GetGetAPIContext();
                          //BraintreeGateway gateways = new BraintreeGateway(
                          //                                                      "client_id$sandbox$rtj8znjyq37kgv4w",
                          //                                                      "client_secret$sandbox$2d25387101c6f7d59babf6d4f4e6a881"
                          //                                                  );
                          //  string url = gateways.OAuth.ConnectUrl(new OAuthConnectUrlRequest
                          //  {
                          //      RedirectUri = "http://localhost:57879/",
                          //      Scope = "read_write",
                          //      State = "foo_state",
                          //      LandingPage = "signup",
                          //      LoginOnly = false,
                          //      User = new OAuthConnectUrlUserRequest
                          //      {
                          //          Country = "CAD",
                          //          Email = "foo@example.com"
                          //      },
                          //      Business = new OAuthConnectUrlBusinessRequest
                          //      {
                          //          Name = "14 Ladders",
                          //          RegisteredAs = "14.0 Ladders"
                          //      },
                          //      PaymentMethods = new string[] { "credit_card", "paypal" }
                          //  });

                          //  var requests = new OAuthCredentialsRequest
                          //  {
                          //      Code = results.Target.Token
                          //  };

                          //  Result<OAuthCredentials> resultss = gateways.OAuth.CreateTokenFromCode(requests);

                          //  string accessToken = resultss.Target.AccessToken;
                          //  DateTime expiresAt = resultss.Target.ExpiresAt.Value;
                          //  string refreshToken = resultss.Target.RefreshToken;
                            //var requests = new TransactionRequest
                            //{
                            //    Amount = (decimal)amount,
                            //    MerchantAccountId= "Canadian",
                            //    CustomerId = customerId,
                            //    PaymentMethodNonce = Token,
                            //    Options = new TransactionOptionsRequest
                            //    {
                            //        SubmitForSettlement = true,
                            //        StoreInVaultOnSuccess = true
                            //    }
                            //};
                            //Result<Braintree.Transaction> resultsss = gateway.Transaction.Sale(requests);
                            if (results.IsSuccess() == true)
                            {
                                List<UserCardDetails> checkdef = nanoDbContext.usercarddetails.Where(s => s.UserId == user.Id.ToString()).ToList();
                                if (checkdef.Count() !=0)
                                {
                                    foreach (var item in checkdef)
                                    {
                                        item.IsDefault = false;
                                        nanoDbContext.Entry(item).State = EntityState.Modified;
                                        nanoDbContext.SaveChanges();
                                    }
                                }
                                UserCardDetails userCardDetails = new UserCardDetails();
                                userCardDetails.UserId = user.Id.ToString();
                                userCardDetails.Token = results.Target.Token;
                                userCardDetails.PayPalEmal = PaypalEmail;
                                userCardDetails.PaypalCustomerId = customerId;
                                userCardDetails.IsDefault = true;
                                nanoDbContext.Entry(userCardDetails).State = EntityState.Added;
                                nanoDbContext.SaveChanges();
                                return true;
                            }
                            return false;
                        }
                    }
                }

                catch (Exception ex)
                {

                    throw;
                }
               
                    
                }
                return false;
            }
        
        public APIContext GetGetAPIContext()
        {
            var config = ConfigManager.Instance.GetProperties();
            string accessToken = new OAuthTokenCredential("AXNWe7wPKRZmGuWCTaniWY3HLQeNs6wMP84BhU81QlqbORQ1_SmKoIkF5S3o8RtqDQVRkkDg0KgdtWaV", "EOiRUtwkgXbGCDl_WzwU7dTM8FNElqPLXCyzMK1zth0bZxA3lACi1K4Y4Bt54e4HD7Af5iBT-aSUBXfC", config).GetAccessToken();
            APIContext apiContext = new APIContext(accessToken);
            apiContext.Config = config;
            return apiContext;
        }
        public bool Payout(double? amount, string currency,string email )
        {
            try
            {
                var apiContext = GetGetAPIContext();
                var payout = new Payout
                {
                    sender_batch_header = new PayoutSenderBatchHeader
                    {
                        sender_batch_id = "batch_" + System.Guid.NewGuid().ToString().Substring(0, 8),
                        email_subject = "NanoJob You have a payment"
                    },
                    items = new List<PayoutItem>
                {
                    new PayoutItem
                    {
                        recipient_type = PayoutRecipientType.EMAIL,
                        amount = new Currency
                        {
                            value = amount.ToString(),
                            currency =currency
                        },
                        receiver = email,
                        note = "Thank you.",
                        sender_item_id = "item_1"
                    }
                }
                };
                try
                {
                    var createdPayout = payout.Create(apiContext, false);
                    return true;
                }
                catch(Exception ex)
                {
                    return false;
                }
            }

            catch (Exception)
            {

                return false;
            }

        }
    }
}
        