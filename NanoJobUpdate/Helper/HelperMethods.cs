﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Nanojobs.Helper
{
    public class HelperMethods
    {
        public bool Getdisance(double lat1, double lon1, double lat2, double lon2, int kiloMeter)
        {
            var R = 6371; // Radius of the earth in km
            var dLat = ToRadians(lat2 - lat1);  // deg2rad below
            var dLon = ToRadians(lon2 - lon1);
            var a =
            Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            Math.Cos(ToRadians(lat1)) * Math.Cos(ToRadians(lat2)) *
            Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; // Distance in km
            return Convert.ToInt32(d) <= kiloMeter;
        }
        public double Getdisance(double lat1, double lon1, double lat2, double lon2)
        {
            var R = 6371; // Radius of the earth in km
            var dLat = ToRadians(lat2 - lat1);  // deg2rad below
            var dLon = ToRadians(lon2 - lon1);
            var a =
            Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            Math.Cos(ToRadians(lat1)) * Math.Cos(ToRadians(lat2)) *
            Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; // Distance in km
            double dist = Convert.ToDouble(d);
            dist = Math.Round(dist, 2);
            return dist;
        }
        private double ToRadians(double deg)
        {
            return (deg * Math.PI / 180.0);
        }
        //public string GetLocation(double lat, double longi)
        //{
        //    var locationService = new GoogleLocationService();
        //    var location = locationService.GetAddressFromLatLang(lat, longi);

        //    if(location.Address != null || location.Address !="")
        //    {
        //        return location.Address;
        //    }
        //    return null;
        //}
    }
}
